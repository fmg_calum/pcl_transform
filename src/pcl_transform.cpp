#include <iostream>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <tf/transform_listener.h>


class PclTransform
{
private:

	ros::NodeHandle     node_handle_;
	ros::Subscriber     points_node_sub_;
	ros::Publisher      transformed_points_pub_;

	std::string         input_point_topic_;
	std::string         target_frame_;
	std::string         output_point_topic_;

	tf::TransformListener *tf_listener_ptr_;

	bool                transform_ok_;

	void publish_cloud(const ros::Publisher& in_publisher,
	                   const sensor_msgs::PointCloud2::ConstPtr &in_cloud_msg)
	{
		in_publisher.publish(in_cloud_msg);
	}

	void transformXYZIRCloud(const sensor_msgs::PointCloud2& in_pclcloud,
	                         sensor_msgs::PointCloud2& out_pclcloud,
	                         const tf::StampedTransform& in_tf_stamped_transform)
	{
		Eigen::Matrix4f transform;

		pcl_ros::transformAsMatrix(in_tf_stamped_transform, transform);
		
		out_pclcloud.header   = in_pclcloud.header;
		out_pclcloud.is_dense = in_pclcloud.is_dense;
		out_pclcloud.width    = in_pclcloud.width;
		out_pclcloud.height   = in_pclcloud.height;
		out_pclcloud.point_step = in_pclcloud.point_step;
		out_pclcloud.row_step = in_pclcloud.row_step;
		out_pclcloud.fields = in_pclcloud.fields;
		out_pclcloud.is_bigendian = in_pclcloud.is_bigendian;
		out_pclcloud.data.reserve (in_pclcloud.data.size ());
		out_pclcloud.data.assign (in_pclcloud.data.begin (), in_pclcloud.data.end ());

		float X ;
		float Y ;
		float Z ;

		float t_X ;
		float t_Y ;
		float t_Z ;

		int arrayPosition, arrayPosX, arrayPosY, arrayPosZ;
		// Go through every point (total number of points = width*height)
		for (size_t i = 0; i < (in_pclcloud.width * in_pclcloud.height); ++i)
		{
			arrayPosition = i*in_pclcloud.point_step;
			//int arrayPosition = y*in_pclcloud.row_step + x*in_pclcloud.point_step;
			arrayPosX = arrayPosition + in_pclcloud.fields[0].offset; // X has an offset of 0
			arrayPosY = arrayPosition + in_pclcloud.fields[1].offset; // Y has an offset of 4
			arrayPosZ = arrayPosition + in_pclcloud.fields[2].offset; // Z has an offset of 8

			memcpy(&X, &in_pclcloud.data[arrayPosX], sizeof(float));
			memcpy(&Y, &in_pclcloud.data[arrayPosY], sizeof(float));
			memcpy(&Z, &in_pclcloud.data[arrayPosZ], sizeof(float));

			if (!pcl_isfinite (X) ||
				!pcl_isfinite (Y) ||
				!pcl_isfinite (Z))
				{continue;}
			//out_cloud.points[i].getVector3fMap () = transform * in_cloud.points[i].getVector3fMap ();
			Eigen::Matrix<float, 3, 1> pt (X, Y, Z);

			t_X = static_cast<float> (transform (0, 0) * pt.coeffRef (0) +
										transform (0, 1) * pt.coeffRef (1) +
										transform (0, 2) * pt.coeffRef (2) +
										transform (0, 3));
			t_Y = static_cast<float> (transform (1, 0) * pt.coeffRef (0) +
										transform (1, 1) * pt.coeffRef (1) +
										transform (1, 2) * pt.coeffRef (2) +
										transform (1, 3));
			t_Z = static_cast<float> (transform (2, 0) * pt.coeffRef (0) +
									  transform (2, 1) * pt.coeffRef (1) +
									  transform (2, 2) * pt.coeffRef (2) +
									  transform (2, 3));

			memcpy(&out_pclcloud.data[arrayPosX], &t_X, sizeof(float));
			memcpy(&out_pclcloud.data[arrayPosY], &t_Y, sizeof(float));
			memcpy(&out_pclcloud.data[arrayPosZ], &t_Z, sizeof(float));
			
		}


	}

	void CloudCallback(const sensor_msgs::PointCloud2::ConstPtr &in_sensor_cloud)
	{
		sensor_msgs::PointCloud2::Ptr transformed_cloud_ptr (new sensor_msgs::PointCloud2);

		bool do_transform = false;
		tf::StampedTransform transform;
		if (target_frame_ != in_sensor_cloud->header.frame_id)
		{
			try {
				tf_listener_ptr_->lookupTransform(target_frame_, in_sensor_cloud->header.frame_id, ros::Time(0),
				                                  transform);
				do_transform = true;
			}
			catch (tf::TransformException ex) {
				ROS_ERROR("cloud_transformer: %s NOT Transforming.", ex.what());
				do_transform = false;
				transform_ok_ = false;
			}
		}
		if (do_transform)
		{
			transformXYZIRCloud(*in_sensor_cloud, *transformed_cloud_ptr, transform);
			transformed_cloud_ptr->header.frame_id = target_frame_;
			if (!transform_ok_)
				{ROS_INFO("cloud_transformer: Correctly Transformed"); transform_ok_=true;}
		}
		else
			{ //pcl::copyPointCloud(*in_sensor_cloud, *transformed_cloud_ptr);
			}

		publish_cloud(transformed_points_pub_, transformed_cloud_ptr);
	}

public:
  PclTransform(tf::TransformListener* in_tf_listener_ptr):node_handle_("~"), transform_ok_(false)
  {
    tf_listener_ptr_ = in_tf_listener_ptr;
  }
  void Run()
  {
    ROS_INFO("Initializing PCL Transformer, please wait...");
    node_handle_.param<std::string>("input_point_topic", input_point_topic_, "/baraja/sensorhead1");
    ROS_INFO("Input point_topic: %s", input_point_topic_.c_str());

    node_handle_.param<std::string>("target_frame", target_frame_, "base_link");
    ROS_INFO("Target Frame in TF (target_frame) : %s", target_frame_.c_str());

    node_handle_.param<std::string>("output_point_topic", output_point_topic_, "/baraja_h1");
    ROS_INFO("output_point_topic: %s", output_point_topic_.c_str());

    ROS_INFO("Subscribing to... %s", input_point_topic_.c_str());
    points_node_sub_ = node_handle_.subscribe(input_point_topic_, 1, &PclTransform::CloudCallback, this);

    transformed_points_pub_ = node_handle_.advertise<sensor_msgs::PointCloud2>(output_point_topic_, 2);

    ROS_INFO("Ready");

    ros::spin();

  }

};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "pcl_transform");
  tf::TransformListener tf_listener;
  PclTransform app(&tf_listener);

  app.Run();

  return 0;

}
